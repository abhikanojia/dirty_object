class Array
  def last=(value)
    self[index(last)] = value
  end
end

# change
module ClassMethods
  def define_dirty_attributes(*args)
    initialize_class
    args.each do |attribute|
      instance_variable = "@#{attribute}"
      create_instance_reader_method(attribute, instance_variable)
      create_instance_writer_method(attribute, instance_variable)
      create_last_attribute_method(attribute)
    end
  end

  def initialize_class
    class_eval do
      def initialize
        @temp_hash = Hash.new(Array.new(2))
        @dirty_hash_change = false
        @dirty_database = Hash.new
        @changed_attributes = []
      end

      private

      def delete_attr_from_hash(symbol)
        # p "delete attribute"
        @temp_hash.delete(symbol)
      end

      def add_new_change_to_hash(symbol, value)
        @temp_hash[symbol] += [value]
        @temp_hash[symbol].shift if @temp_hash[symbol].size > 2
      end

      def change_status_to_true
        @dirty_hash_change = true
      end

      def change_status_to_false
        @dirty_hash_change = false
      end

      def shift_values
        @temp_hash.each { |key, values| values.shift if @temp_hash[key].size > 2 }
      end

      def push_data_to_temp_hash(key, value)
        @temp_hash[key] += [value]
      end

      def dirty_db_has_value?(key, value)
        @dirty_database[key].eql? value
      end

      def save_object
        push_to_dirty_db
        change_status_to_false
        @changed_attributes.clear
      end

      def push_to_dirty_db
        @temp_hash.each { |key, value| @dirty_database[key] = value.last }
      end

      def push_to_changed_attributes(attribute)
        @changed_attributes.push(attribute)
      end

      def show_changes
        @temp_hash.select { |attr| @changed_attributes.include? attr }
      end

      def remove_key_from_changed_attributes(attr)
        @changed_attributes.delete(attr)
      end

      def update_value_of_attr(attribute, value)
        @temp_hash[attribute].last = value
      end
    end
  end

  private

  def create_last_attribute_method(attribute)
    define_method("#{attribute}_was") do
      p @temp_hash[attribute].first
    end
  end

  def create_instance_reader_method(attribute, instance_variable)
    define_method("#{attribute}") do
      instance_variable_get instance_variable
    end
  end

  def create_instance_writer_method(attribute, instance_variable)
    define_method("#{attribute}=") do |value|
      if dirty_db_has_value?(attribute.to_sym, value)
        update_value_of_attr(attribute.to_sym, value)
        change_status_to_false
        remove_key_from_changed_attributes(attribute.to_sym)
      else
        push_to_changed_attributes(attribute.to_sym)
        push_data_to_temp_hash(attribute.to_sym, value) if @temp_hash[attribute].last != value
        update_value_of_attr(attribute.to_sym, value)
        change_status_to_true
        shift_values
      end
      instance_variable_set instance_variable, value
    end
  end
end

module DirtyObject
  def self.included(klass)
    klass.extend(ClassMethods)
  end

  def changes
    p show_changes
  end

  def changed?
    p @dirty_hash_change
  end

  def save
    save_object
  end

  def method_missing(name)
    puts "Undefined method....."
  end
end
