# require dirty object
require_relative '../lib/dirty_object.rb'
# User class includes dirty object
class User
  include DirtyObject
  attr_accessor :name, :age, :email
  define_dirty_attributes :name, :age
end

# u = User.new

# p u.name # nil
# p u.age # nil

# u.changes # {}

# u.name = nil
# u.age = nil

# u.changes # {}


# u.name = 'TEST'
# u.age = 30

# u.changes # {:name=>[nil, "TEST"], :age=>[nil, 30]}

# u.save



# u.name = 'TEST'
# # p "here0"
# u.age = 303
# # p "here1"


# u.changes # {:name=>[nil, "TEST"], :age=>[nil, 30]}

# u.save


# u.changes # {}



u = User.new
u.name = 'Akhil'
u.age = 30

u.changed?
u.changes


u.name_was
u.email_was
u.age_was

u.save

u.changed?
u.changes


u.name = 'New name'
u.age = 31

u.changes
u.name_was

u.name = 'Akhil'
u.changes
u.changed?

u.age = 30
u.changes

u.changed?
